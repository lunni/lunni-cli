from lunni_cli import __version__

def version():
    """
    Display version information about Lunni CLI.
    """

    print("lunni-cli version {version}".format(version=__version__))
